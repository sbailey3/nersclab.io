# NERSC Technical Documentation

[National Energy Research Scientific Computing
(NERSC)](https://www.nersc.gov) provides High Performance Computing
and Storage facilities and support for research sponsored by, and of
interest to, the U.S. Department of Energy (DOE) Office of Science
(SC).

## Top documentation pages

* [Getting Started](getting-started.md) - Information for new and existing users
* [Getting Help](getting-started.md#getting-help) - How to get support
* [Job Queue Policy](jobs/policy.md) - Charge factors, run limits, submit limits
* [Example Jobs](jobs/examples/index.md) - Curated example job scripts
* [Jobs overview](jobs/index.md) - Slurm commands, job script basics, submitting, updating jobs
* [Jupyter](services/jupyter.md) - Interactive jupyter notebooks at NERSC
* [Globus](services/globus.md) - High-performance data transfers
* [File permissions](filesystems/unix-file-permissions.md) - Unix file permissions
* [Multi-Factor Authentication](connect/mfa.md)

## Computing Resources

* [Perlmutter](systems/perlmutter/index.md) - A Cray EX system with AMD CPUs and NVIDIA A100 GPUs
* [Cori](systems/cori/index.md) - A Cray XC40 system with Intel Haswell and KNL CPUs

## Other NERSC web pages

* [NERSC Home page](https://nersc.gov) - center news and information
* [MOTD](https://www.nersc.gov/live-status/motd/) - live status
* [MyNERSC](https://my.nersc.gov) - interactive content
* [Help Portal](https://help.nersc.gov) - open support tickets, make requests
* [JupyterHub](https://jupyter.nersc.gov) - access NERSC with interactive notebooks and more
* [Iris](https://iris.nersc.gov) - account management

!!! tip
    The [NERSC Users Group (NUG)](https://www.nersc.gov/users/NUG/) is an
    independent organization of users of NERSC resources.

    All users are welcome to [join the NUG Slack
    workspace](https://www.nersc.gov/users/NUG/nersc-users-slack/).

!!! info "NERSC welcomes your contributions"
    These pages are hosted from a [git
    repository](https://gitlab.com/NERSC/nersc.gitlab.io) and
    [contributions](https://gitlab.com/NERSC/nersc.gitlab.io/blob/main/CONTRIBUTING.md)
    are welcome!

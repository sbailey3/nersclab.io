#include <CL/sycl.hpp>

#include <cmath>
#include <iostream>

namespace sycl = cl::sycl;

int main() {
  const int n = 100000;
  const sycl::range<1> m{n};

  sycl::buffer<double, 1> b_a{n}, b_b{n}, b_c{n};

  {
    auto a = b_a.get_access<sycl::access::mode::discard_write>();
    auto b = b_b.get_access<sycl::access::mode::discard_write>();    
    for (size_t i = 0; i < n; i++) {
      a[i] = sin(i)*sin(i);
      b[i] = cos(i)*cos(i);
    }
  }
  
  sycl::queue q{sycl::gpu_selector{}};
  
  q.submit([&](sycl::handler& h) {
      auto a = b_a.get_access<sycl::access::mode::read>(h);
      auto b = b_b.get_access<sycl::access::mode::read>(h);
      auto c = b_c.get_access<sycl::access::mode::write>(h);
      
      h.parallel_for<class xpy>(m, [=](sycl::id<1> i) {
	  c[i] = a[i] + b[i];
	});
    });
  
  {
    double sum = 0.0;
    auto c = b_c.get_access<sycl::access::mode::read>();
    for (size_t i=0; i<n; i++) sum += c[i];
    std::cout << "sum = " << sum/n << std::endl;
    
    if ( fabs(sum - static_cast<double>(n)) <= 1.0e-8 ) {
      return 0;
    } else {
      return 1;
    }
    
  }  
  return 0;
}

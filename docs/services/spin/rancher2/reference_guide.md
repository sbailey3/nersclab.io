# Spin Reference Guide (Rancher 2)

## Maintenance and Troubleshooting

### Viewing logs

### Viewing performance metrics

## Network

### Making a Workload Accessible on the Network

## Best Practices

Below are some general best practices which apply to all kinds of Spin
workloads.

### Do not store personally identifiable information in Spin

Many Spin workloads employ databases together with web UIs to ingest, store,
and present data curated to specific users or communities. However, Spin users
should be aware that Berkeley Lab policy prohibits storage of [personally
identifiable information
(PII)](https://commons.lbl.gov/display/cpp/Information+to+Protect#test-15889462)
in Spin.

### Use SSL certificates

Publicly accessible federal websites, including any website hosted in Spin,
must use [HTTP Strict Transport Security (HSTS)](https://https.cio.gov/guide/),
which uses HTTPS to encrypt all network traffic between the website server and
client, even when the client requests only unencrypted HTTP traffic.

In order to comply with federal requirements, the ingress controller in Rancher
2 is already configured to use HTTPS for all external network traffic, even
when the user does not provide their own [SSL
certificate](https://rancher.com/docs/rancher/v2.5/en/k8s-in-rancher/certificates/)
to their workload. However, when a user-provided SSL certificate is absent from
a workload, the ingress controller uses a default certificate instead, which
does not contain all of the appropriate metadata for a particular workload.
*Spin users are therefore strongly encouraged to supply their own signed SSL
certificates for all website-based workloads.*

### Understand visibility and scope of workloads

In Rancher 2, components of a workload are accessible to all users who are
members of the same project under which the workload exists. This includes
environment variables,
[secrets](https://rancher.com/docs/rancher/v2.x/en/k8s-in-rancher/secrets/),
and most other elements and configurations of the workload. It is currently not
possible to narrow the scope of these components further than an entire
project. Spin user who prefer for other members of their project not to have
access to elements of their workloads should take this behavior into
consideration when designing their Spin workflows.
